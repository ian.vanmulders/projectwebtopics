onmessage = function (event) {
    console.log('Received number from main thread:', event.data);

    let result;
    // Perform computation
    setTimeout(() => {
        result = event.data;
            postMessage(result);

    }, 2000)

    // Send result back to the main thread
};