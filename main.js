const allArticleLinks = document.querySelectorAll('.article a');


allArticleLinks.forEach(link => {
    link.addEventListener("click", (event) => {
        event.preventDefault();


        const myWorker = new Worker('worker.js');

        myWorker.postMessage("Ik ben deze pagina aan het laden voor jou");

        myWorker.onmessage = function (event) {
            console.log('Return boodschap van worker.js', event.data);
        };

        const articleId = link.getAttribute('id');
        const workerMessage = "I'm loading this page for you";
        const article = document.querySelector(`#${articleId}`);
        const pTag = document.createElement('p');
        pTag.textContent = workerMessage;
        pTag.classList.add("created");
        article.appendChild(pTag);

        setTimeout(() => {
            let refLink = link.getAttribute('href');
            article.removeChild(pTag);
            location.href = refLink;
        }, 5000);

    });
});
