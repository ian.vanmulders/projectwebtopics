Voor dit project van web Topics heb ik gebruik gemaakt van verschillende topics. 
Ik heb geprobeerd om verschillende topics te verwerken in mijn project namelijk 
- Web Worker
- Three.js
- OpenLayers
- RSS feed

Niet kunnen implementeren
- Web Components

Om te beginnen heb ik bij de Web Worker ervoor gezorgd dat ik in de console een boodschap kan laten zien 
die na 2 seconden zegt dat hij een boodschap laat zien dat hij de volgende pagina voor jou aan het laden is.

THREE.JS 
Om te laten zien dat ik toch iets met three.js kan maken heb ik op een paar pagina's een rondraaiende
bol gezet. Hierbij was het niet altijd even gemakkelijk om het de juiste posities te vinden maar ook om deze te laten draaien
was ook een uitdaging. Bij deze is opzoekwerk en verder zoeken een vereiste geweest. De bedoeling was dat ik deze ging steken in een 
Web Component maar hier ben ik niet geraakt. Ik heb deze wel gemaakt in een javascript file maar niet op de juiste manier kunnen implementeren.

OPENLAYERS
Bij de verschillende artikels kan je ook onderaan een map terugvinden die je laat zien waar de activiteit zich afspeelt.
Deze plaats staan op de kaart aangeduid met een marker zodat je niet te veel tijd meer moet verspillen om te zoeken op de kaart. 
Hierbij vond ik het jammer dat ik de workshop van OpenLayers en Three.js had gemist want ik wou hier wel verder mee gaan dan dat ik gekomen ben.

RSS FEED
Als laatste heb ik ook een RSS feed gemaakt waar de 5 artikels opstaan. Op deze RSS feed kan je de titel, publicatie datum, etc terugvinden.
Je ziet hier ook als de nieuwe artikels er op gezet worden. Dus als er iets nieuw op de feed wordt gezet kan je dit ook zien.$

Moest alles werken zou je op volgende link alles moeten kunnen zien.
https://projectwebtopics.ianvanmulders.ikdoeict.be/

(Hier werkt jammer genoeg niet alles maar op local host kan ik jullie wel alles laten zien.)