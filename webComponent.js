class RotatingObject extends HTMLElement {
    constructor() {
        super();
        const shadow = this.attachShadow({ mode: 'open' });

        const container = document.createElement('div');
        container.classList.add('container');

        const rotatingThing = document.createElement('div');
        rotatingThing.classList.add('object');

        container.appendChild(rotatingThing);
        shadow.appendChild(container);
    }
}
customElements.define('rotatingObject', RotatingObject);
window.customElements.define('rotatingObject', RotatingObject);

