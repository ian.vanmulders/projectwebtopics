import * as THREE from 'three'
import {OrbitControls} from 'https://unpkg.com/three@0.163.0/examples/jsm/controls/OrbitControls.js';

//scene
    const scene = new THREE.Scene();

//create sphere
    const geometry = new THREE.SphereGeometry(3, 25, 25)
// const geometry = new THREE.BoxGeometry(3, 3, 3)
    const material = new THREE.MeshStandardMaterial({
        color: 0xff0000,
        roughness: 0.4,
        metalness: 0.5,
    })
// const material = new THREE.MeshPhongMaterial({
//     color: 0xff8ff8,
//     shininess: 100,
// })

    const mesh = new THREE.Mesh(geometry, material)
    scene.add(mesh)

//scene.add(meshEdges);

//sizes
    const sizes = {
        width: window.innerWidth / 2,
        height: window.innerHeight / 2,
    }

//light
    const light = new THREE.PointLight(0xffffff, 100, 100)
    light.position.set(5, 5, 10)
    scene.add(light)

//camera
    const camera = new THREE.PerspectiveCamera(45, sizes.width / sizes.height, 1, 100)
    camera.position.z = 20
    scene.add(camera)

//renderer
    const canvas = document.querySelector('#sphere')
    const renderer = new THREE.WebGLRenderer({canvas, alpha: true})
// document.body.appendChild(renderer.domElement);
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(2)
    renderer.render(scene, camera)

//controls
    const controls = new OrbitControls(camera, canvas)
    controls.enableDamping = true
    controls.enablePan = false
    controls.enableZoom = false
    controls.autoRotate = true
    controls.autoRotateSpeed = 5


//resize
    window.addEventListener("resize", () => {
        //update sizes
        sizes.width = window.innerWidth
        sizes.height = window.innerHeight
        //update camera
        camera.aspect = sizes.width / sizes.height
        camera.updateProjectionMatrix()
        renderer.setSize(sizes.width, sizes.height)
    })


    function animate() {
        controls.update()
        requestAnimationFrame(animate);
        renderer.render(scene, camera)
    }

    animate();
