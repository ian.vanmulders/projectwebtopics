import "./style.css";
import { Map, Overlay, View } from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import Style from "ol/style/Style";
import Icon from "ol/style/Icon";
import Feature from "ol/Feature";
import { fromLonLat } from "ol/proj";
import { Point, LineString, Polygon } from "ol/geom";


const netherlandsCoords = fromLonLat([5.1214201, 52.0907374]);

const netherlandsMarker = new Feature({
    geometry: new Point(netherlandsCoords),
});

const markerStyle = new Style({
    image: new Icon({
        src: "https://openlayers.org/en/latest/examples/data/icon.png",
        size: [32, 48],
        offset: [0, 0],
        anchor: [0.5, 1],
    }),
});
netherlandsMarker.setStyle(markerStyle);

const map1 = new Map({
    target: "map1",
    layers: [
        new TileLayer({
            source: new OSM(),
        }),
        new VectorLayer({
            source: new VectorSource({
                features: [netherlandsMarker],
            }),
        }),
    ],
    view: new View({
        center: netherlandsCoords,
        zoom: 11,
    }),
});

